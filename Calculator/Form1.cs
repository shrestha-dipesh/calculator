﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        private bool dotAllowed = false;
        private bool calculationPerformed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void numeric_button(object sender, EventArgs e)
        {
            Button btn = (Button) sender;
            if (txtContainer.Text == "0")
            {
                txtContainer.Text = btn.Text;
            }
            else
            {
                if (calculationPerformed)
                {
                    txtContainer.Text = btn.Text;
                    resultContainer.Text = "";
                    calculationPerformed = false;
                }
                else
                {
                    txtContainer.Text += btn.Text;
                }
            }
        }

        private void ac_Click(object sender, EventArgs e)
        {
            txtContainer.Text = "0";
            resultContainer.Text = "";
            calculationPerformed = false;
        }

        private void backspace_Click(object sender, EventArgs e)
        {
            txtContainer.Text = txtContainer.Text.Remove(txtContainer.Text.Length - 1);
            if (txtContainer.Text.Length == 0)
            {
                txtContainer.Text = "0";
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (!txtContainer.Text.Contains(".") || dotAllowed)
            {
                txtContainer.Text += ".";
                dotAllowed = false;
            }
        }

        private void operator_button(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (calculationPerformed)
            {
                txtContainer.Text = resultContainer.Text;
                resultContainer.Text = "";
                calculationPerformed = false;
            }
            if (txtContainer.Text != "0" && !(txtContainer.Text.Contains("+") || txtContainer.Text.Contains("-") || txtContainer.Text.Contains("x") || txtContainer.Text.Contains("/")))
            {
                txtContainer.Text += btn.Text;
                dotAllowed = true;
            }
        }

        private void equals_button(object sender, EventArgs e)
        {
            if (txtContainer.Text.Contains("+"))
            {
                string[] num = txtContainer.Text.Split('+');
                resultContainer.Text = (float.Parse(num[0]) + float.Parse(num[1])).ToString();
            }
            else if (txtContainer.Text.Contains("-"))
            {
                string[] num = txtContainer.Text.Split('-');
                resultContainer.Text = (float.Parse(num[0]) - float.Parse(num[1])).ToString();
            }
            else if (txtContainer.Text.Contains("x"))
            {
                string[] num = txtContainer.Text.Split('x');
                resultContainer.Text = (float.Parse(num[0]) * float.Parse(num[1])).ToString();
            }
            else if (txtContainer.Text.Contains("/"))
            {
                string[] num = txtContainer.Text.Split('/');
                resultContainer.Text = (float.Parse(num[0]) / float.Parse(num[1])).ToString();
            }
            calculationPerformed = true;

        }

        private void percent_Click(object sender, EventArgs e)
        {
            if (calculationPerformed)
            {
                txtContainer.Text = resultContainer.Text;
            }
            try
            {
                resultContainer.Text = (float.Parse(txtContainer.Text) / 100).ToString();
                txtContainer.Text += "%";
                calculationPerformed = true;
            }
            catch (Exception ex) { }
        }
    }
}
